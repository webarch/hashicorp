# Ansible Hashicorp repo role 

[![pipeline status](https://git.coop/webarch/hashicorp/badges/main/pipeline.svg)](https://git.coop/webarch/hashicorp/-/commits/main)

Ansible role to install HashiCorp packages on Debian and Ubuntu.

## Terraform

This role has only been tested for [installing Terraform](https://www.terraform.io/docs/cli/install/apt.html).

See the [defaults/main.yml](defaults/main.yml), if you don't want to install the latest version then set `version` to one of the versions listed when you run `apt policy terraform` or `apt-cache madison terraform`.
